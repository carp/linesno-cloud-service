package com.alinesno.cloud.base.boot.feign.dto;

import com.alinesno.cloud.common.facade.feign.BaseDto;

/**
 * <p> 传输对象</p>
 *
 * @author LuoAnDong
 * @since 2019-04-08 08:14:52
 */
@SuppressWarnings("serial")
public class ManagerAccountRoleDto extends BaseDto {

	private Boolean accountId;
	
	private String roleId;
	


	public Boolean isAccountId() {
		return accountId;
	}

	public void setAccountId(Boolean accountId) {
		this.accountId = accountId;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

}
