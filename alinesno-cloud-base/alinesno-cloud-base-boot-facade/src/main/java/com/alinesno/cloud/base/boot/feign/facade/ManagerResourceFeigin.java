package com.alinesno.cloud.base.boot.feign.facade;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.alinesno.cloud.base.boot.feign.dto.ManagerResourceDto;
import com.alinesno.cloud.common.facade.feign.IBaseFeign;

/**
 * <p>  请求客户端 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:02:27
 */
@FeignClient(name="alinesno-cloud-base-boot" , path="managerResource")
public interface ManagerResourceFeigin extends IBaseFeign<ManagerResourceDto> {

	/**
	 * 查询出一级菜单 
	 * @param resourceParent
	 * @return
	 */
	@GetMapping("findMenus")
	ManagerResourceDto findMenus(@RequestParam("resourceParent") String resourceParent);
	
	/**
	 * 查询菜单
	 * @param resourceParent
	 * @param applicationId
	 * @return
	 */
	@GetMapping("findMenusByApplication")
	ManagerResourceDto findMenusByApplication(@RequestParam("resourceParent") String resourceParent , @RequestParam("applicationId") String applicationId);

	/**
	 * 查询账号菜单 
	 * @param resourceParent
	 * @param applicationId
	 * @param accountId
	 * @return
	 */
	@GetMapping("findMenusByApplicationAndAccount")
	ManagerResourceDto findMenusByApplicationAndAccount(@RequestParam("resourceParent") String resourceParent, @RequestParam("applicationId") String applicationId, @RequestParam("accountId")  String accountId);

}
