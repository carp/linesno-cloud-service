package com.alinesno.cloud.base.boot.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@Entity
@Table(name="info_zipcode")
public class InfoZipcodeEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

	private String owners;
	private String area;
	@Column(name="zip_code")
	private String zipCode;


	public String getOwners() {
		return owners;
	}

	public void setOwners(String owners) {
		this.owners = owners;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}


	@Override
	public String toString() {
		return "InfoZipcodeEntity{" +
			"owners=" + owners +
			", area=" + area +
			", zipCode=" + zipCode +
			"}";
	}
}
