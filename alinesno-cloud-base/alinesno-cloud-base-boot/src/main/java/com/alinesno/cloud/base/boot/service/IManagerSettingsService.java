package com.alinesno.cloud.base.boot.service;

import org.springframework.data.repository.NoRepositoryBean;

import com.alinesno.cloud.base.boot.entity.ManagerSettingsEntity;
import com.alinesno.cloud.base.boot.repository.ManagerSettingsRepository;
import com.alinesno.cloud.common.core.services.IBaseService;

/**
 * <p> 参数配置表 服务类 </p>
 *
 * @author WeiXiaoJin
 * @since 2019-07-06 15:47:49
 */
@NoRepositoryBean
public interface IManagerSettingsService extends IBaseService<ManagerSettingsRepository, ManagerSettingsEntity, String> {

}
