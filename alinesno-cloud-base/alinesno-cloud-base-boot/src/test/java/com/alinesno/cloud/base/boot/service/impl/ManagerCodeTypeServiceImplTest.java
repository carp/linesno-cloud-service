package com.alinesno.cloud.base.boot.service.impl;

import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alinesno.cloud.base.boot.entity.ManagerCodeTypeEntity;
import com.alinesno.cloud.base.boot.service.IManagerCodeTypeService;
import com.alinesno.cloud.common.core.junit.JUnitBase;

public class ManagerCodeTypeServiceImplTest extends JUnitBase {

	@Autowired
	private IManagerCodeTypeService managerCodeTypeService ; 
	
	@Test
	public void testFindAll() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllSort() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllById() {
		fail("Not yet implemented");
	}

	@Test
	public void testSaveAll() {
	
		List<ManagerCodeTypeEntity> l = new ArrayList<ManagerCodeTypeEntity>() ; 
		
		ManagerCodeTypeEntity e1 = new ManagerCodeTypeEntity() ; 
		e1.setCodeTypeName("公共状态");
		e1.setCodeTypeValue("common_status");
		l.add(e1) ; 
		
		ManagerCodeTypeEntity e2 = new ManagerCodeTypeEntity() ; 
		e2.setCodeTypeName("管理员类型");
		e2.setCodeTypeValue("manager_type");
		l.add(e2) ; 
		
		managerCodeTypeService.saveAll(l) ; 
	}

	@Test
	public void testFlush() {
		fail("Not yet implemented");
	}

	@Test
	public void testSaveAndFlush() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteInBatch() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteAllInBatch() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetOne() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllExampleOfS() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllExampleOfSSort() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllPageable() {
		fail("Not yet implemented");
	}

	@Test
	public void testSave() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindById() {
		fail("Not yet implemented");
	}

	@Test
	public void testExistsById() {
		fail("Not yet implemented");
	}

	@Test
	public void testCount() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteById() {
		fail("Not yet implemented");
	}

	@Test
	public void testDelete() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteAllIterableOfQextendsEntity() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteAll() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindOneExampleOfS() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllExampleOfSPageable() {
		fail("Not yet implemented");
	}

	@Test
	public void testCountExampleOfS() {
		fail("Not yet implemented");
	}

	@Test
	public void testExists() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindOneSpecificationOfEntity() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllSpecificationOfEntity() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllSpecificationOfEntityPageable() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllSpecificationOfEntitySort() {
		fail("Not yet implemented");
	}

	@Test
	public void testCountSpecificationOfEntity() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteByIds() {
		fail("Not yet implemented");
	}

}
