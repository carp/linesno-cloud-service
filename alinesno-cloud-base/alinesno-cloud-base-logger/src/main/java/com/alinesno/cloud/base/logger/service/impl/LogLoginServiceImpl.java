package com.alinesno.cloud.base.logger.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.alinesno.cloud.base.logger.entity.LogLoginEntity;
import com.alinesno.cloud.base.logger.repository.LogLoginRepository;
import com.alinesno.cloud.base.logger.service.ILogLoginService;
import com.alinesno.cloud.common.core.services.impl.IBaseServiceImpl;

/**
 * <p>  服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:16:06
 */
@Service
public class LogLoginServiceImpl extends IBaseServiceImpl<LogLoginRepository, LogLoginEntity, String> implements ILogLoginService {

	//日志记录
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(LogLoginServiceImpl.class);

}
