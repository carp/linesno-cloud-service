package com.alinesno.cloud.base.logger.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.alinesno.cloud.base.logger.entity.LogRecordEntity;
import com.alinesno.cloud.base.logger.repository.LogRecordRepository;
import com.alinesno.cloud.base.logger.service.ILogRecordService;
import com.alinesno.cloud.common.core.services.impl.IBaseServiceImpl;

/**
 * <p>  服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:16:06
 */
@Service
public class LogRecordServiceImpl extends IBaseServiceImpl<LogRecordRepository, LogRecordEntity, String> implements ILogRecordService {

	//日志记录
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(LogRecordServiceImpl.class);

}
