package com.alinesno.cloud.base.message.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;


/**
 * 定时任务
 * 
 * @author LuoAnDong
 * @since 2018年12月17日 下午10:20:51
 */
public class AsyncTaskService {

	// 日志记录
	private static final Logger log = LoggerFactory.getLogger(AsyncTaskService.class);

	@Async
	public void dataTranslate(int i) {
		log.info("启动了线程" + i);
	}

}