package com.alinesno.cloud.base.print.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.alinesno.cloud.base.print.entity.TemplateEntity;
import com.alinesno.cloud.base.print.repository.TemplateRepository;
import com.alinesno.cloud.base.print.service.ITemplateService;
import com.alinesno.cloud.common.core.services.impl.IBaseServiceImpl;

/**
 * <p>  服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2019-05-03 14:07:38
 */
@Service
public class TemplateServiceImpl extends IBaseServiceImpl<TemplateRepository, TemplateEntity, String> implements ITemplateService {

	//日志记录
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(TemplateServiceImpl.class);

}
