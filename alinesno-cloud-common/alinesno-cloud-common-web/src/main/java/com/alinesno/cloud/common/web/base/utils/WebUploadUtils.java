package com.alinesno.cloud.common.web.base.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import cn.hutool.core.lang.UUID;

/**
 * 文件上传工具对象，用于单体应用的，内置的简单文件上传功能
 * 
 * @author LuoAnDong
 * @since 2019年4月11日 上午8:42:51
 */
@Component
public class WebUploadUtils {

	private static final Logger log = LoggerFactory.getLogger(WebUploadUtils.class);

	@Value("${storage.web.local.upload-path:user.home}")
	private String logFileTmp; // 本地文件

	/**
	 * 保存到本地临时目录
	 * 
	 * @param file
	 * @param fileName
	 * @return
	 */
	public String storageFile(MultipartFile file, String fileName) {
		try {
			String localPath = new SimpleDateFormat("yyyy/MM/dd/").format(new Date());
			String tempFolder = logFileTmp + File.separator + localPath;
			File f = new File(tempFolder);
			if (!f.exists()) {
				FileUtils.forceMkdir(new File(tempFolder));
			}

			String newFilename = UUID.randomUUID() + fileName.substring(fileName.lastIndexOf("."));

			log.debug("上传目录:{} , file.getOriginalFilename():{}", tempFolder, file.getOriginalFilename());
			Path path = Paths.get(tempFolder + File.separator + newFilename);

			FileUtils.copyInputStreamToFile(file.getInputStream(), path.toFile());

			log.debug("path = {}", path.toFile().getAbsolutePath());
			return path.toFile().getAbsolutePath();
		} catch (IOException e) {
			log.error("图片上传错误:{}", e);
		}

		return null;
	}

}