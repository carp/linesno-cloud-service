package com.alinesno.cloud.common.web.enable;

import java.util.List;

import com.alinesno.cloud.common.core.auto.CoreImportProvider;
import com.alinesno.cloud.common.web.base.advice.DatagridResponseBodyAdvice;
import com.alinesno.cloud.common.web.base.advice.plugins.ApplicationPlugin;
import com.alinesno.cloud.common.web.base.advice.plugins.DirectoryPlugin;
import com.alinesno.cloud.common.web.base.controller.PageJumpController;
import com.alinesno.cloud.common.web.base.controller.WebStorageController;
import com.alinesno.cloud.common.web.base.exceptions.GlobalExceptionHandler;
import com.alinesno.cloud.common.web.base.form.FormTokenAop;
import com.alinesno.cloud.common.web.base.thymeleaf.TagsDialect;
import com.alinesno.cloud.common.web.base.utils.WebUploadUtils;
import com.alinesno.cloud.common.web.base.xss.XssConfig;
import com.alinesno.cloud.common.web.login.aop.AccountRecordAspect;
import com.alinesno.cloud.common.web.login.controller.CommonController;
import com.alinesno.cloud.common.web.login.controller.DashboardController;
import com.alinesno.cloud.common.web.login.session.RedisSessionConfig;

/**
  *   公共 web包提供接口
 * 
 * @author WeiXiaoJin
 * @since 2019年7月14日 下午5:10:48
 */
public class WebImportProvider {

	/**
	 * 提供接口对象
	 * 
	 * @return
	 */
	public static List<String> classLoader() {

		// common core
		List<String> coreLoader = CoreImportProvider.classLoader();
		coreLoader.addAll(coreLoader);

		// common web
		coreLoader.add(FormTokenAop.class.getName());
		coreLoader.add(GlobalExceptionHandler.class.getName());
		coreLoader.add(WebUploadUtils.class.getName());
		coreLoader.add(TagsDialect.class.getName());
		coreLoader.add(WebStorageController.class.getName());
		coreLoader.add(PageJumpController.class.getName());

		coreLoader.add(DatagridResponseBodyAdvice.class.getName());
		coreLoader.add(DirectoryPlugin.class.getName());
		coreLoader.add(ApplicationPlugin.class.getName());
		coreLoader.add(AccountRecordAspect.class.getName());

		coreLoader.add(DashboardController.class.getName());
		coreLoader.add(CommonController.class.getName());
		coreLoader.add(RedisSessionConfig.class.getName());

		// xss 过滤
		coreLoader.add(XssConfig.class.getName());

		return coreLoader;
	}

}