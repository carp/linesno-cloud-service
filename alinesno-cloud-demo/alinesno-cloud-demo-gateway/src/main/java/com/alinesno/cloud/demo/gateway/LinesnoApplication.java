package com.alinesno.cloud.demo.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

/**
 * 启动入口
 * 
 * @author LuoAnDong
 * @since 2018年8月7日 上午8:45:02
 */
@Configuration
@SpringBootApplication
public class LinesnoApplication {

	public static void main(String[] args) {
		SpringApplication.run(LinesnoApplication.class, args);
	}

}
