package com.alinesno.cloud.demo.single.web.services;

import com.alinesno.cloud.common.core.services.IBaseService;
import com.alinesno.cloud.demo.single.web.entity.LoggerEntity;
import com.alinesno.cloud.demo.single.web.repository.LoggerRepository;

public interface LoggerService extends IBaseService<LoggerRepository, LoggerEntity, String> {

}
