package com.alinesno.cloud.demo.single.web.services.impl;

import org.springframework.stereotype.Service;

import com.alinesno.cloud.common.core.services.impl.IBaseServiceImpl;
import com.alinesno.cloud.demo.single.web.entity.LoggerEntity;
import com.alinesno.cloud.demo.single.web.repository.LoggerRepository;
import com.alinesno.cloud.demo.single.web.services.LoggerService;

@Service
public class LoggerServiceImpl extends IBaseServiceImpl<LoggerRepository, LoggerEntity, String> implements LoggerService {


}
