package com.alinesno.cloud.compoment.code.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 框架技术池
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-06-29 12:19:41
 */
@Entity
@Table(name="frameworks")
public class FrameworksEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 技术编码
     */
	private String code;
    /**
     * 技术名称
     */
	private String name;
    /**
     * 技术描述
     */
	private String description;


	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	@Override
	public String toString() {
		return "FrameworksEntity{" +
			"code=" + code +
			", name=" + name +
			", description=" + description +
			"}";
	}
}
