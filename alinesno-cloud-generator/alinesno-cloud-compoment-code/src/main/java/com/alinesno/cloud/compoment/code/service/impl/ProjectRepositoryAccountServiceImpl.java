package com.alinesno.cloud.compoment.code.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.alinesno.cloud.common.core.services.impl.IBaseServiceImpl;
import com.alinesno.cloud.compoment.code.entity.ProjectRepositoryAccountEntity;
import com.alinesno.cloud.compoment.code.repository.ProjectRepositoryAccountRepository;
import com.alinesno.cloud.compoment.code.service.IProjectRepositoryAccountService;

/**
 * <p> 版本控制管理 服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2019-06-29 12:19:41
 */
@Service
public class ProjectRepositoryAccountServiceImpl extends IBaseServiceImpl<ProjectRepositoryAccountRepository, ProjectRepositoryAccountEntity, String> implements IProjectRepositoryAccountService {

	//日志记录
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(ProjectRepositoryAccountServiceImpl.class);

}
