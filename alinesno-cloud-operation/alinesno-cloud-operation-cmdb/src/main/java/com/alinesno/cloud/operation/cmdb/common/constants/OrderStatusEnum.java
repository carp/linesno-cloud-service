package com.alinesno.cloud.operation.cmdb.common.constants;

/**
 * 订单状态
 * 
 * @author LuoAnDong
 * @since 2018年8月5日 下午6:41:23
 */
public enum OrderStatusEnum {

	ORDER_PENDING("01" , "待接单"), // 待接单
	ORDER_RECEIPT("02" , "已接单"), // 已接单
	ORDER_SENDING("03" ,"送单中"), // 送单中
	ORDER_OUTTIME("05" , "订单超时") , // 订单超时
	
	ORDER_FINISH("04" , "已签收"), // 已签收 
	ORDER_DELETE("06" , "已删除"), // 已删除
	ORDER_ADDPAY("07" , "已加价"), // 已加价
	ORDER_SCORE("08" , "已评价"), // 已评价
	ORDER_ERROR("99" ,"订单错误") ; // 订单错误 

	public String code;
	public String text ;
	
	OrderStatusEnum(String code , String text) {
		this.code = code;
		this.text = text ; 
	}
	
	public static OrderStatusEnum getStatus(String code) {
		if("01".equals(code)) {
			return ORDER_PENDING ; 
		}else if("02".equals(code)) {
			return ORDER_RECEIPT ; 
		}else if("03".equals(code)) {
			return ORDER_SENDING ; 
		}else if("04".equals(code)) {
			return ORDER_FINISH ; 
		}else if("05".equals(code)) {
			return ORDER_OUTTIME ; 
		}else if("06".equals(code)) {
			return ORDER_DELETE ; 
		}else if("07".equals(code)) {
			return ORDER_ADDPAY ; 
		}else if("08".equals(code)) {
			return ORDER_SCORE ; 
		}else {
			return ORDER_ERROR ;
		}
	}
	
	public String getText() {
		return this.text ; 
	}

	public String getCode() {
		return this.code;
	}
}