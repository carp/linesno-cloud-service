package com.alinesno.cloud.operation.cmdb.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 管理员账户
 * 
 * @author LuoAnDong
 * @since 2018年8月5日 下午12:20:30
 */
@SuppressWarnings("serial")
@Entity
@Table(name="cmdb_account")
public class AccountEntity extends BaseEntity {

	private String accountStatus; // 账户状态(1正常|0不正常)
	private String lastLoginIp; // 最后登陆ip,
	private String lastLoginTime; // 最后登陆时间,
	
	@Column(unique=true)
	private String loginName; // 登陆名称,
	
	private String password; // 登陆密码,
	private String salt; // 加密字符,
	private String userId; // 用户信息id,
	private String roleId; // 所属角色,
	private String name; // 用户名称.,
	private String rolePower; // 用户权限(1租户权限/0用户权限/9超级管理员)
	
	public String getAccountStatus() {
		return accountStatus;
	}
	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}
	public String getLastLoginIp() {
		return lastLoginIp;
	}
	public void setLastLoginIp(String lastLoginIp) {
		this.lastLoginIp = lastLoginIp;
	}
	public String getLastLoginTime() {
		return lastLoginTime;
	}
	public void setLastLoginTime(String lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getSalt() {
		return salt;
	}
	public void setSalt(String salt) {
		this.salt = salt;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRolePower() {
		return rolePower;
	}
	public void setRolePower(String rolePower) {
		this.rolePower = rolePower;
	}
	
}
