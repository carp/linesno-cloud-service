package com.alinesno.cloud.operation.cmdb.entity;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-01-23
 */
@SuppressWarnings("serial")
@Entity
@Table(name="cmdb_sms_template")
public class SmsTemplateEntity extends BaseEntity {
	 
	/**
	 * 模板内容 
	 */
	@Lob //大字段
	private String templateContent ; 
	
	/**
	 * 模板id
	 */
	private String templateId ; 
	
	/**
	 * 模板描述 
	 */
	private String templateDesc ;

	
	public SmsTemplateEntity() {
		super();
	}

	public SmsTemplateEntity(String templateContent, String templateId, String templateDesc) {
		super();
		this.templateContent = templateContent;
		this.templateId = templateId;
		this.templateDesc = templateDesc;
		this.setAddTime(new Timestamp(System.currentTimeMillis()));
	}

	public String getTemplateId() {
		return templateId;
	}

	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	public String getTemplateContent() {
		return templateContent;
	}

	public void setTemplateContent(String templateContent) {
		this.templateContent = templateContent;
	}

	public String getTemplateDesc() {
		return templateDesc;
	}

	public void setTemplateDesc(String templateDesc) {
		this.templateDesc = templateDesc;
	} 


}
