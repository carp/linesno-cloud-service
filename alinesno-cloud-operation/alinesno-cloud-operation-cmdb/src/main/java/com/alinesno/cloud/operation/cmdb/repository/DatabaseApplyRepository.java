package com.alinesno.cloud.operation.cmdb.repository;

import com.alinesno.cloud.operation.cmdb.entity.DatabaseApplyEntity;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface DatabaseApplyRepository extends BaseJpaRepository<DatabaseApplyEntity, String> {
	
}