package com.alinesno.cloud.operation.cmdb.repository;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import com.alinesno.cloud.operation.cmdb.entity.EmailEntity;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface EmailRepository extends BaseJpaRepository<EmailEntity, String> {

	/**
	 * 通过邮件查询信息
	 * @param email
	 */
	List<EmailEntity> findByEmail(String email);

	/**
	 * 查询为状态为0的数据
	 * @param value
	 * @param of
	 * @return
	 */
	Page<EmailEntity> findAllByRendStatus(int rendStatus , Pageable pageable);

	/**
	 * 根据日期查询一天最大的量
	 * @param value
	 * @param date
	 * @return
	 */
	@Query("select count(1) from EmailEntity t1 where t1.rendStatus=?1 and date_format(updateTime,'%Y-%m-%d') =?2")
	int countByRendStatusAndDate(int rendStatus , String date);
	
}