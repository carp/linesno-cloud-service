package com.alinesno.cloud.operation.cmdb.repository;
import java.util.List;

import org.springframework.data.domain.Pageable;

import com.alinesno.cloud.operation.cmdb.entity.MachineEntity;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface MachineRepository extends BaseJpaRepository<MachineEntity, String> {

	List<MachineEntity> findAllByMachineStatusAndMasterCode(String machineStatus, String masterCode, Pageable pageable);

	MachineEntity findByMachineIp(String machineIp);

}