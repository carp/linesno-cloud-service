package com.alinesno.cloud.operation.cmdb.repository;
import org.springframework.data.jpa.repository.Query;

import com.alinesno.cloud.operation.cmdb.entity.OrderInfoEntity;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface OrderInfoRepository extends BaseJpaRepository<OrderInfoEntity, String>  {

	@Query("select count(1) as c from OrderInfoEntity t1 where to_days(t1.addTime) = to_days(now())")
	int dateCount();

	 
}