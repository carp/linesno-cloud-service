package com.alinesno.cloud.operation.cmdb.repository;
import java.util.Collection;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.alinesno.cloud.operation.cmdb.entity.OrdersEntity;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface OrderRepository extends BaseJpaRepository<OrdersEntity, String> {

	List<OrdersEntity> findAllByUserIdAndSendStatusInOrderByAddTimeDesc(String userId , Collection<String> sendStatus) ; 
	
	List<OrdersEntity> findAllByReceiveManagerIdAndSendStatus(String receiveManagerId , String sendStatus) ; 

	Page<OrdersEntity> findAllByMasterCode(String masterCode, Pageable pageable);

	List<OrdersEntity> findBySendStatus(String sendStatus);
	
	List<OrdersEntity> findBySendStatusAndSalaIdIsNotNull(String sendStatus);

	OrdersEntity findByOrderId(String orderId);

}