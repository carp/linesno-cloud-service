package com.alinesno.cloud.operation.cmdb.repository;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.alinesno.cloud.operation.cmdb.entity.ScoreEntity;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface ScoreRepository extends BaseJpaRepository<ScoreEntity, String> {

	/**
	 * 通过用户和订单查询订单信息
	 * @param orderId
	 * @param id
	 * @return
	 */
	@Query("from ScoreEntity t where t.orderId=?1 and  t.userId=?2") 
	List<ScoreEntity> findByOrderIdAndUserId(@Param("orderId") String orderId,@Param("id") String id);

}