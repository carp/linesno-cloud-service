package com.alinesno.cloud.operation.cmdb.web.api;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.alinesno.cloud.operation.cmdb.common.constants.ParamsEnum;
import com.alinesno.cloud.operation.cmdb.common.constants.RunConstants;
import com.alinesno.cloud.operation.cmdb.entity.UserEntity;
import com.alinesno.cloud.operation.cmdb.service.ParamsService;
import com.alinesno.cloud.operation.cmdb.web.bean.CurrentUserBean;
import com.alinesno.cloud.operation.cmdb.web.bean.ResponseBean;
import com.alinesno.cloud.operation.cmdb.web.session.PageSessionUtils;

/**
 * 公共父类
 * @author LuoAnDong
 * @since 2018年8月5日 下午1:03:00
 */
public abstract class ApiParent {
	
	public ResponseBean<String> responseB = new ResponseBean<String>() ; 
	
	@Autowired
	private HttpSession session ; 
	
	@Autowired
	private HttpServletRequest request; 
	
	@Autowired
	public ParamsService paramsService ; 

	@Autowired
	private PageSessionUtils pageSessionUtils ;
	 
	public int getOrderOutTime() {
		int o = paramsService.findParamByNameToInt(ParamsEnum.ORDER_OUTTIME.getCode() , getUser().getMasterCode()) ; 
		return o ; 
	}
	
	/**
	 * 获取到当前登陆用户
	 * @return
	 */
	public CurrentUserBean getUser() {
		return pageSessionUtils.pageSession(request) ; 
	}

	/**
	 * 设置当前用户Session
	 * @param e
	 * @return
	 */
	public CurrentUserBean setCurrentSession(UserEntity e) {
		CurrentUserBean bean = new CurrentUserBean() ; 
		BeanUtils.copyProperties(e, bean); 
		session.setAttribute(RunConstants.CURRENT_USER, bean) ; 
		return bean ; 
	}

	/**
	 * 业务处理返回 
	 * @param request
	 * @param response
	 * @return
	 */
	public abstract Object handler(HttpServletRequest request , HttpServletResponse response) ; 
	
}
