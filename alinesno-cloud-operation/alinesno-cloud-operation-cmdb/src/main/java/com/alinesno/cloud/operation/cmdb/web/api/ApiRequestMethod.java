package com.alinesno.cloud.operation.cmdb.web.api;
/**
 * 接口数据请求的类型
 * @author LuoAnDong
 * @date 2017年12月1日 下午9:01:56
 */
public enum ApiRequestMethod {
	SOCKET("socket") , 
	WEBSERVER("webserver") , 
	HTTP("http") , 
	POST("post") , 
	GET("get")  ; 

	private String value ; 
	
	ApiRequestMethod(String value) {
		this.value = value ; 
	}
	
	String value() {
		return value ; 
	}
}