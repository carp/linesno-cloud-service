package com.alinesno.cloud.operation.cmdb.web.bean;

import java.io.Serializable;

/**
 * 当前登陆用户
 * @author LuoAnDong
 * @since 2018年8月17日 上午7:49:08
 */
@SuppressWarnings("serial")
public class AccountBean implements Serializable{
	
	private String accountStatus; // 账户状态(1正常|0不正常)
	private String lastLoginIp; // 最后登陆ip,
	private String lastLoginTime; // 最后登陆时间,
	private String loginName; // 登陆名称,
	private String salt; // 加密字符,
	private String id ; // 用户信息id,
	private String roleId; // 所属角色,
	private String name; // 用户名称.,
	private String rolePower; // 用户权限(1租户权限/0用户权限),
	private String masterCode ; //数据权限 

	public String getMasterCode() {
		return masterCode;
	}
	public void setMasterCode(String masterCode) {
		this.masterCode = masterCode;
	}
	public String getAccountStatus() {
		return accountStatus;
	}
	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}
	public String getLastLoginIp() {
		return lastLoginIp;
	}
	public void setLastLoginIp(String lastLoginIp) {
		this.lastLoginIp = lastLoginIp;
	}
	public String getLastLoginTime() {
		return lastLoginTime;
	}
	public void setLastLoginTime(String lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getSalt() {
		return salt;
	}
	public void setSalt(String salt) {
		this.salt = salt;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRolePower() {
		return rolePower;
	}
	public void setRolePower(String rolePower) {
		this.rolePower = rolePower;
	}
	
}
