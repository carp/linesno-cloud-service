package com.alinesno.cloud.platform.stack.email.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.ResourceUtils;

import com.alinesno.cloud.operation.cmdb.common.constants.EmailSendStatus;
import com.alinesno.cloud.operation.cmdb.common.util.RunStringUtils;
import com.alinesno.cloud.operation.cmdb.entity.EmailEntity;
import com.alinesno.cloud.operation.cmdb.third.email.EmailService;
import com.alinesno.cloud.operation.cmdb.web.bean.ResponseBean;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EmailServiceImplTest {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private EmailService emailService;

	@Test
	public void testSendSingleEmailStringStringString() {
		String email = "landonniao@163.com";
		String subject = "测试邮件发送";
		String htmlBody = "测试邮件发送";
		ResponseBean<String> b = emailService.sendSingleEmail(email, subject, htmlBody);
		logger.debug("result = {}", b);
	}

	

	@Test
	public void testSendSingleEmailStringArrayStringString() throws FileNotFoundException {
		String email[] = new String[] { "landonniao@163.com", "luoandon@gmail.com", "958346765@qq.com" , "1484191824@qq.com" , "1727710007@qq.com" , "1066233729@qq.com" };

		File file = ResourceUtils.getFile("classpath:email/invite_email.html");

		String subject = "恭喜你,获取【民大AD云主机】开张邀请函";
		String htmlBody = RunStringUtils.readToString(file) ; 
		logger.debug("html body = {}", htmlBody);

		 ResponseBean<String> b = emailService.sendSingleEmail(email, subject,htmlBody);
		 logger.debug("result = {}", b);
	}
	
	@Test
	public void testFindLimit() {
		List<EmailEntity> es = emailService.findLimit(10) ; 
		for(EmailEntity e: es) {
			logger.debug("list = {}" , ToStringBuilder.reflectionToString(e));
		}
	}
	
	@Test
	public void testFindDaySend() {
		int count = emailService.findDaySend(EmailSendStatus.SEND) ; 
		logger.debug("发送的邮件数量:{}" , count);
	}

}
