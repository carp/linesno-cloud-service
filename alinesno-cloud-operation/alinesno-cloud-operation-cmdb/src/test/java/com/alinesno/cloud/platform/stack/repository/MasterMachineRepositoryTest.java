package com.alinesno.cloud.platform.stack.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.alinesno.cloud.operation.cmdb.entity.MasterMachineEntity;
import com.alinesno.cloud.operation.cmdb.repository.MasterMachineRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MasterMachineRepositoryTest {

	@Autowired
	private MasterMachineRepository masterMachineRepository ; 

	@Test
	public void testSave() {
		MasterMachineEntity e = new MasterMachineEntity() ; 
		e.setMasterName("广西民族大学");
		e.setMasterCode("10608");
		
		masterMachineRepository.save(e) ; 
	}

}
