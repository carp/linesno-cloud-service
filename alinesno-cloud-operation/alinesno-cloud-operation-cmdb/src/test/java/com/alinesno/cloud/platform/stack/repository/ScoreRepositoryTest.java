package com.alinesno.cloud.platform.stack.repository;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.alinesno.cloud.operation.cmdb.entity.ScoreEntity;
import com.alinesno.cloud.operation.cmdb.repository.ScoreRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ScoreRepositoryTest {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass()) ; 

	@Autowired
	private ScoreRepository scoreRepository ; 
	
	@Test
	public void testFindByOrderIdAndUserId() {
		List<ScoreEntity> e = scoreRepository.findByOrderIdAndUserId("asdf", "1") ; 
		logger.debug("e = {}" , e);
	}

}
