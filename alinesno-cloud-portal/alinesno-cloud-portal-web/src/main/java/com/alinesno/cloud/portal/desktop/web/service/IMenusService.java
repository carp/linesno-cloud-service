package com.alinesno.cloud.portal.desktop.web.service;

import java.util.List;

import org.springframework.data.repository.NoRepositoryBean;

import com.alinesno.cloud.common.core.services.IBaseService;
import com.alinesno.cloud.portal.desktop.web.bean.MenusBean;
import com.alinesno.cloud.portal.desktop.web.entity.MenusEntity;
import com.alinesno.cloud.portal.desktop.web.repository.MenusRepository;

/**
 * <p>  服务类 </p>
 *
 * @author LuoAnDong
 * @since 2019-05-18 14:46:04
 */
@NoRepositoryBean
public interface IMenusService extends IBaseService<MenusRepository, MenusEntity, String> {

	/**
	 * 查询主导航
	 * @return
	 */
	List<MenusBean> findNavigation();

}
