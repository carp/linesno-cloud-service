package com.alinesno.cloud.base.boot.web.utils;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alinesno.cloud.base.boot.feign.dto.ManagerSourceGenerateDto;
import com.alinesno.cloud.compoment.generate.Generator;

/**
 * 代码生成类
 * 
 * @author LuoAnDong
 * @since 2019年4月22日 上午6:53:35
 */
public class GeneratorSourceTool extends Generator {
	
	private static final Logger log = LoggerFactory.getLogger(GeneratorSourceTool.class);

	private ManagerSourceGenerateDto dto ; 
	private String generatorPath ; 
	
	public GeneratorSourceTool(ManagerSourceGenerateDto dto , String generatorPath) {
		this.dto = dto;
		this.generatorPath = generatorPath ; 
	}

	@Override
	protected void initParam(String dataSourceType) {
		super.initParam(dataSourceType);

		log.debug("generatorPath:{}" , generatorPath);
		
		// 启动服务
		feiginServer = dto.getFeignServerPath() ; 
		parentPackage= dto.getPackageName() ; 
		bootPrefix = dto.getBootPrefix() ; 
		springApplicationName = feiginServer ; 
				
		abcPath = generatorPath ; 
		gc.setOutputDir(generatorPath) ; 
		gc.setAuthor(dto.getAuthorName()) ; 
		gc.setOpen(false) ;
		
		// 数据库源
		dsc.setDriverName(dto.getDbDriver()) ; 
		dsc.setUsername(dto.getDbUser()) ; 
		dsc.setPassword(dto.getDbPwd()) ; 
		dsc.setUrl(dto.getDbUrl()) ; 
		
		springApplicationName = feiginServer ; 
		serverPort = StringUtils.isBlank(serverPort)?"24001":serverPort ; 
		groupId = parentPackage + "." + buildModelName(moduleName,feiginServer).replace("/", ".") ; 
		articleId = feiginServer ; 
		
	}

	@Override
	public void initTableName() {

	}

}