package com.alinesno.cloud.base.storage.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alinesno.cloud.base.storage.feign.dto.StorageFileDto ;
import com.alinesno.cloud.base.storage.feign.facade.StorageFileFeigin ;

import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import com.alinesno.cloud.common.web.base.advice.TranslateCode;
import com.alinesno.cloud.common.web.base.bean.DatatablesPageBean;
import com.alinesno.cloud.common.web.base.controller.FeignMethodController;

import org.springframework.stereotype.Controller;

/**
 * <p> 前端控制器 </p>
 *
 * @author LuoAnDong
 * @since 2019-06-20 22:29:10
 */
@Controller
@Scope(SpringInstanceScope.PROTOTYPE)
@RequestMapping("base/storage/storageFile")
public class StorageFileController extends FeignMethodController<StorageFileDto, StorageFileFeigin> {

	//日志记录
	private static final Logger log = LoggerFactory.getLogger(StorageFileController.class);

	@TranslateCode(value="[{hasStatus:has_status}]")
	@ResponseBody
	@PostMapping("/datatables")
    public DatatablesPageBean datatables(HttpServletRequest request , Model model ,  DatatablesPageBean page){
		log.debug("page = {}" , ToStringBuilder.reflectionToString(page));
		return this.toPage(model, feign , page) ;
    }

	/**
	 * 查看监控 
	 * @param applicationId
	 * @return
	 */
	@RequestMapping("/monitor")
	public void monitor() {
		log.debug("监控页面.");
	}
}


























